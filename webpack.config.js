module.exports = {
    devtool: 'source-map',
    entry: {
        chart: './src/chart.js',
        'chart-socket': './src/chart-socket.js',
        main: './src/app.js',
        post: './src/post.js',
        socket: './src/socket.js',
        'try-catch': './src/try-catch.js',
        update: './src/update.js'
    },
    mode: 'development',
    module: {
        rules: [{
            exclude: /node_modules/,
            use: [{
                loader: 'babel-loader'
            }, {
                loader: 'prettier-loader'
            }],
            test: /\.js$/
        }]
    },
    output: {
        filename: '[name].bundle.js',
        path: __dirname + '/dist'
    }
}