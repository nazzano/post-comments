const { Chat } = require("./classes/chat");

const sck = new WebSocket(
  "wss://demo.piesocket.com/v3/channel_1?api_key=oCdCMcMPQpbvNjUIzqtvF1d2X2okWpDQj4AwARJuAgtjhzKxVEjQU6IdCjwm&notify_self"
);

const form = document.querySelector('form[name="edit-comment"]');
const message = document.querySelector('input[name="message"]');
const div = document.getElementById("messaggio");

sck.addEventListener("message", ev => {
  div.innerText = ev.data + div.innerText;
});

div.innerText = form.addEventListener("submit", e => {
  e.preventDefault();

  sck.send(message.value);
});

const messages = document.querySelector('textarea[name="messages"]');

const chat = new Chat(
  "wss://demo.piesocket.com/v3/channel_1?api_key=oCdCMcMPQpbvNjUIzqtvF1d2X2okWpDQj4AwARJuAgtjhzKxVEjQU6IdCjwm&notify_self",
  ev => {
    messages.value += `\r\n${ev.data}`;
  }
);
