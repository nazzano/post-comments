
const messages = [];

export const createChat = (url, onMessage) => {
    const socket = new WebSocket(url);

    socket.addEventListener("message", ev => {
      messages.push(ev.data);
      onMessage(ev);
    });

    return socket;
};

export const createChatWithPromise = (url, onMessage) => {
    return new Promise((resolve, reject) => {
        const socket = new WebSocket(url);

        socket.addEventListener("message", ev => {
          messages.push(ev.data);
          onMessage(ev);
        });
    
        return resolve(socket);
    });
};

export const getMessages = () => [...messages];

export const sendMessage = (chat, message) => {
    chat.send(message);
}