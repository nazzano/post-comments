import { addComment, deleteComment, getComments } from "./functions/comments";
import { getPost } from "./functions/posts";

const addCommentForm = document.querySelector('form[name="add-comment"]');

// Comments 1. Individuo gli elementi nel codice HTML
const commentsSection = document.querySelector("#comments");

// Post 1. Individuo gli elementi nel codice HTML
const postContent = document.querySelector(".post > p");
const postTitle = document.querySelector(".post > h1");

const appendComment = c => {
  const row = document.createElement("div");
  // Esempio di set / lettura di un data attribute
  // La nomenclatura dash case di HTML viene convertita in JavaScript in camelCase
  //
  // row.setAttribute('data-tuo-attributo');
  // row.dataset.tuoAttributo

  row.className = "row";

  row.innerHTML = `
      <div class="col-lg-12">
          <h3>${c.name}</h3>
          <p>${c.body}</p>
          <p>Scritto da: ${c.email}</p>
      </div>
  `;

  // <span class="delete-comment" data-id="${c.id}">X</span>
  const span = document.createElement("span");
  span.className = "delete-comment";
  span.setAttribute("data-id", c.id);
  span.innerText = "X";

  span.addEventListener("click", async e => {
    // Tutti i data attributes vengono agglomerati in una chiave "dataset" presente sull'elemento HTML
    // Se creiamo un data-attribute
    console.log(e.target.dataset.id);

    // L'id del commento posso ricavarmelo in due modi:
    // 1. Sfruttando il data attribute "id" => e.target.dataset.id
    // 2. Tenendo in considerazione che tutte le funzioni in JS sono closure, quindi anche nella funzione
    //    di questo ascoltatore posso accedere al parametro "c" che contiene l'oggetto Comment
    const deletedComment = await deleteComment(e.target.dataset.id);

    console.log(deletedComment);

    // Rimuovo la riga corrispondente al commento rimosso dal backend
    // Per farlo, ho nuovamente due modi:
    // 1. Individuo la riga del commento cercando lo span con un particolare data-id (che coincide
    //    con l'id del commento da eliminare)
    // 2. Sfrutto le closure: se devo rimuovere la riga, invoce il metodo remove() dalla costante row

    // Se non ottengo un oggetto (codice diverso da 200) la risorsa non è stata rimossa dal backend
    // quindi non la rimuovo dall'interfaccia
    if (!deletedComment) {
      return;
    }

    row.remove();
  });

  row.firstElementChild.firstElementChild.append(span);

  commentsSection.append(row);
};

const readComments = async () => {
  const comments = await getComments(1);

  comments.forEach(c => appendComment(c));
};

const readPost = async () => {
  const post = await getPost(1);

  postContent.innerHTML = post.body;
  postTitle.innerHTML = post.title;
};

// Eventualmente, al posto di usare una funzione async per caricare
// i dati: getPost(1).then(...)

readPost();
readComments();

addCommentForm.addEventListener("submit", async e => {
  e.preventDefault();

  // Raccogliere i dati da inviare al backend
  const content = document.querySelector('input[name="content"]').value;
  const email = document.querySelector('input[name="email"]').value;
  const name = document.querySelector('input[name="name"]').value;

  // Aggiungi il commento e preleva la risposta dal backend
  const c = await addComment(email, name, content, 1);

  appendComment(c);
});
