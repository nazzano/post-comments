import Chart from "chart.js/auto";

function initchart() {
  return (chart = new Chart(ctx, {
    type: "line",
    data: {
      labels,
      datasets: [
        {
          label: "My First Dataset",
          data,
          fill: false,
          borderColor: "rgb(75, 192, 192)",
          tension: 0.1,
        },
      ],
    },
    options: {
      animation: false,
      scales: {
        x: {
          beginAtZero: true,
          max: 140,
        },
        y: {
          beginAtZero: true,
          max: 100,
        },
      },
    },
  }));
}

//let lastYear = 1960;

const ctx = document.getElementById("chart").getContext("2d");

let chart = initchart();
let data = [];
let i = 1;
let labels = [];

setInterval(() => {
  // Parte iniziale e finale del grafico
  if (i <= 20 || i >= 120) {
    data.push(50);
  } else if (i > 45 && i <= 95) {
    // esle if: discesa
    // data[i - 1] preleva l'ultimo elemento nell'array
    // De i  conta i punti ... (es.)
    //    - Sono al 45esimo punto
    //    - Nell'array avrò posizioni da 0 a 44 (45 elementi)
    //    - Prelevo la posizione 44 che contiene l'ultimo numero (i = 45; i - 1 => 44)
    data.push(data[data.length - 1] - 1);
  } else {
    data.push(data[data.length - 1] + 1);
  }
  labels.push(i);
  chart.update();

  if (i === 140) {
    console.log(data);
    i = 0;
    data = [];
    labels = [];
    chart.destroy();
    chart = initchart();
  }

  i++;
}, 100);

//setInterval(() => {
//  data.push(+(Math.random() * 100).toFixed());
//  labels.push(++lastYear);
//  chart.update();
//}, 1000);
