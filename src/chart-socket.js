import Chart from "chart.js/auto";

function initchart() {
  return (chart = new Chart(ctx, {
    type: "line",
    data: {
      labels,
      datasets: [
        {
          label: "My First Dataset",
          data,
          fill: false,
          borderColor: "rgb(75, 192, 192)",
          tension: 0.1,
        },
      ],
    },
    options: {
      animation: false,
      scales: {
        x: {
          beginAtZero: true,
          max: 140,
        },
        y: {
          beginAtZero: true,
          max: 100,
        },
      },
    },
  }));
}

const chart = initchart(ctx);

const sck = new WebSocket("ws://127.0.0.1:9000");

sck.addEventListener("message", ev => {
  const dt = JSON.parse(ev.data);

  data.push(+dt.value);
});
