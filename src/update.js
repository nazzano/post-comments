import { getComment, putComment } from "./functions/comments";

const form = document.querySelector('form[name="update-comment"]');

const content = document.querySelector('input[name="content"]');
const email = document.querySelector('input[name="email"]');
const id = document.querySelector('input[name="id"]');
const name = document.querySelector('input[name="name"]');
const postId = document.querySelector('input[name="post-id"]');

const load = async () => {
  const comment = await getComment(3);

  content.value = comment.body;
  email.value = comment.email;
  id.value = comment.id;
  name.value = comment.name;
  postId.value = comment.postId;
};

load();

form.addEventListener("submit", async e => {
  e.preventDefault();

  const comment = await putComment(
    id.value,
    email.value,
    name.value,
    content.value,
    postId.value
  );

  console.log(comment);
});
