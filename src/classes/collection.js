import { IndexRelatedError } from "../errors/collection-errors";

export class Collection {
  #_elements = [];

  add(e) {
    this.#_elements.push;
  }

  get(i) {
    const e = this.#_elements[i];

    if (e) {
      return e;
    }

    // Gli errori (eccezioni) si sollevano (rise) o lanciano (throw)
    // l'istruzione da usare è di solito throw, e nel gergo informatico si dice
    // che "to rise an exception"
    //
    // Gli errori sono oggetti istanza Error
    // throw new Error('Element not found for the given id');
    //
    // Quando si scatena l'errore?
    //   - get prende un indice
    //   - l'indice viene usato per prelevare un elemento dall'array #_elements
    //   - se l'elemento non esiste (non c'è un elemento alla posizione i) lancio un errore
    throw new IndexRelatedError("Element not found for the given index");
  }
}
