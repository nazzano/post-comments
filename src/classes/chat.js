export class Chat {
  #_messages = [];
  #_socket;

  constructor(url, onMessage) {
    this.#_socket = new WebSocket(url);

    this.#_socket.addEventListener("message", ev => {
      this.#_messages.push(ev.data);
      onMessage(ev);
    });
  }

  getMessages() {
    return [...this.#_messages];
  }

  sendMessage(message) {
    this.#_socket.send(message);
  }
}
